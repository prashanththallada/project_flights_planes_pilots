const express = require('express')
const api = express.Router()

api.get("/flightsStatistics",(req,res)=>{
    res.render("Statistics/flights.ejs");
})

api.get("/pilotsStatistics",(req,res)=>{
    res.render("Statistics/pilots.ejs");
})

api.get("/planesStatistics",(req,res)=>{
    res.render("Statistics/planes.ejs");
})

module.exports = api