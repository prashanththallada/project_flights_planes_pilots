const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------





// GET t4
api.get('/t4', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t4/index.ejs',
        { title: 'TeamName', layout: 'layout.ejs' })
})
api.get('/t4/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t4/a/index(2).ejs',
        { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
})

// api.get('/css', (req,res)=>{
//   res.render('about')
// })

api.get('/t4/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t4/b/cheruku.ejs',
        { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
})
api.get('/t4/c', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t4/c/index.ejs',
        { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
})


module.exports = api
